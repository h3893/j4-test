class CfgPatches
{
    class item_test
    {
        name = "Item Test";
        author = "N. Harrison";
        requiredAddons[] = {"a3_weapons_f","ace_common","ace_interact_menu"};
        units[] = {};
        weapons[] = {"iTest_bandolier_556","iTest_bandolier_762_NATO","iTest_bandolier_762_AK"};
    };
};

class CfgFunctions
{
    class iTest
    {
        class functions
        {
            file = "item_test\functions";
            class bandolier {};
        };
    };
};

class CfgVehicles
{
    class Man;
    class CAManBase: Man
    {
        class ACE_SelfActions 
        {
            class ACE_Equipment
            {
                class bandolier_556
                {
                    displayName = "Unpack Bandolier (5.56x45mm)";
                    condition = "'iTest_bandolier_556' in items player";
                    statement = "'iTest_bandolier_556' call iTest_fnc_bandolier";
                };
                class bandolier_762_NATO
                {
                    displayName = "Unpack Bandolier (7.62x51mm)";
                    condition = "'iTest_bandolier_762_NATO' in items player";
                    statement = "'iTest_bandolier_762_NATO' call iTest_fnc_bandolier";
                };
                class bandolier_762_AK
                {
                    displayName = "Unpack Bandolier (7.62x39mm)";
                    condition = "'iTest_bandolier_762_AK' in items player";
                    statement = "'iTest_bandolier_762_AK' call iTest_fnc_bandolier";
                };
            };
        };
    };
};

class CfgWeapons
{
    class ACE_ItemCore;
    class CBA_MiscItem_ItemInfo;
    class iTest_bandolier_556: ACE_ItemCore
    {
        scope = 2;
        displayName = "Bandolier (5.56x45mm)";
        descriptionShort = "A bandolier containing four 5.56x45mm STANAG magazines.";

        class ItemInfo: CBA_MiscItem_ItemInfo
        {
            mass = 32; //mass of magazine x # of magazines; 4x 5.56 STANAG magazines
        };
    };
    class iTest_bandolier_762_NATO: ACE_ItemCore
    {
        scope = 2;
        displayName = "Bandolier (7.62x51mm)";
        descriptionShort = "A bandolier containing three 7.62x51mm magazines.";

        class ItemInfo: CBA_MiscItem_ItemInfo
        {
            mass = 36; //mass of magazine x # of magazines; 3x 7.62 NATO magazines
        };
    };
    class iTest_bandolier_762_AK: ACE_ItemCore
    {
        scope = 2;
        displayName = "Bandolier (7.62x39mm)";
        descriptionShort = "A bandolier containing five 7.62x39mm magazines.";

        class ItemInfo: CBA_MiscItem_ItemInfo
        {
            mass = 50; //mass of magazine x # of magazines; 5x 7.62 AK magazines
        };
    };
};
