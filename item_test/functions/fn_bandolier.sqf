params ["_item"];

switch (_item) do
{
    case "iTest_bandolier_556":
    { 
        [3, [], {player removeItem 'iTest_bandolier_556'; player addMagazines ['30Rnd_556x45_Stanag', 4]}, {}, "Unpacking bandolier."] call ace_common_fnc_progressBar;
    };
    case "iTest_bandolier_762_NATO":
    {
        [3, [], {player removeItem 'iTest_bandolier_762_NATO'; player addMagazines ['20Rnd_762x51_Mag', 3]}, {}, "Unpacking bandolier."] call ace_common_fnc_progressBar;
    };
    case "iTest_bandolier_762_AK":
    {
        [3, [], {player removeItem 'iTest_bandolier_762_AK'; player addMagazines ['30Rnd_762x39_AK12_Mag_F', 5]}, {}, "Unpacking bandolier."] call ace_common_fnc_progressBar;
    };
};
